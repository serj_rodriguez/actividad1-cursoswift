//
//  EmployeeController.swift
//  Actividad1
//
//  Created by Macbook on 20/05/19.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class EmployeeController: UIViewController{
    
    let empleado : Empleado = Empleado.sharedInstance
    let calendar = Calendar.current
    
    @IBOutlet weak var detailTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        detailTableView.delegate = self
        detailTableView.dataSource = self
        detailTableView.register(UINib(nibName: "DetailCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
}

extension EmployeeController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return empleado.registros.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : DetailCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DetailCell
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from:empleado.registros[indexPath.row].strBirthday)!
        
        let today = calendar.startOfDay(for: Date())
        let dayAndMonth = calendar.dateComponents([.day, .month], from: date)
        let nextBirthDay = calendar.nextDate(after: today, matching: dayAndMonth,
                                        matchingPolicy: .nextTimePreservingSmallerComponents)!
        
        let diff = calendar.dateComponents([.day], from: today, to: nextBirthDay)
        print(diff.day!)
        
        if diff.day! > 5{
                cell.bDayImage.image = nil
                cell.isUserInteractionEnabled = false
        }
        
        cell.nameLabel.text = empleado.registros[indexPath.row].strNombre
        
        return cell
    }
    
    
}

extension EmployeeController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}
