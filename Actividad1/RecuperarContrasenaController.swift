//
//  RecuperarContrasenaController.swift
//  Actividad1
//
//  Created by Macbook on 20/05/19.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class RecuperarContrasenaController: UIViewController {
    
    let empleado: Empleado = Empleado.sharedInstance

    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        
        view.endEditing(true)
    }
    
    @IBAction func confirmButtonTapped(_ sender: Any) {
        
        if emailTextField.text == empleado.strEmail {
            //Enviar alerta de usuario o password incorrecto
            let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                self.navigationController?.popToRootViewController(animated: true)
            }
            let alert = UIAlertController(title: "Aviso", message: "Hemos enviado tu nuevo password a tu correo electronico", preferredStyle: .alert)
            alert.addAction(acceptAction)
            self.present(alert, animated: true) {
            }
        }else if emailTextField.text == ""{
            //Enviar alerta de usuario o password incorrecto
            let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            }
            let alert = UIAlertController(title: "Aviso", message: "Campo email obligatorio", preferredStyle: .alert)
            alert.addAction(acceptAction)
            self.present(alert, animated: true) {
            }
        }else{
            //Enviar alerta de usuario o password incorrecto
            let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            }
            let alert = UIAlertController(title: "Aviso", message: "Email incorrecto", preferredStyle: .alert)
            alert.addAction(acceptAction)
            self.present(alert, animated: true) {
            }
        }
    }
    
}
