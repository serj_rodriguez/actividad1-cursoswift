//
//  RegistroController.swift
//  Actividad1
//
//  Created by Macbook on 20/05/19.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class RegistroController: UIViewController {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usrTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var birthDayTextField: UITextField!
    @IBOutlet weak var employeeTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var repPwdTextField: UITextField!
    
    var empleado : Empleado = Empleado.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        
        view.endEditing(true)
    }

    @IBAction func birthdayTextFieldTapped(_ sender: UITextField) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        birthDayTextField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(RegistroController.datePickerFromValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func registerButtonTapped(_ sender: UIButton) {
        
        if nameTextField.text == "" || emailTextField.text == "" || birthDayTextField.text == "" || employeeTextField.text == "" || phoneTextField.text == "" || pwdTextField.text == ""  || repPwdTextField.text == ""{
            
            //Enviar alerta de usuario o password incorrecto
            let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            }
            let alert = UIAlertController(title: "Aviso", message: "Todos los campos son obligatorios", preferredStyle: .alert)
            alert.addAction(acceptAction)
            self.present(alert, animated: true) {
            }
        }else if !isValidEmail(str: emailTextField.text ?? "") {
            print("invalid email")
            //Enviar alerta de usuario o password incorrecto
            let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            }
            let alert = UIAlertController(title: "Aviso", message: "Email invalido", preferredStyle: .alert)
            alert.addAction(acceptAction)
            self.present(alert, animated: true) {
            }
        }else if pwdTextField.text != repPwdTextField.text{
            
            let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            }
            let alert = UIAlertController(title: "Aviso", message: "Las contraseñas no coinciden", preferredStyle: .alert)
            alert.addAction(acceptAction)
            self.present(alert, animated: true) {
            }
        }else{
            
            var tmp : Empleado = Empleado()
            tmp.strNombre = nameTextField.text
            tmp.strUser = usrTextField.text
            tmp.strEmail = emailTextField.text
            tmp.strBirthday = birthDayTextField.text
            tmp.strEmpNumber = employeeTextField.text
            tmp.strPhone = phoneTextField.text
            tmp.strPwd = pwdTextField.text
            
            if usrExists(usr: usrTextField.text ?? ""){
                
                let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                }
                let alert = UIAlertController(title: "Aviso", message: "El nombre de usuario ya existe", preferredStyle: .alert)
                alert.addAction(acceptAction)
                self.present(alert, animated: true) {
                }
            }else if emailExists(email: emailTextField.text ?? ""){
                let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                }
                let alert = UIAlertController(title: "Aviso", message: "El email ya se encuentra registrado", preferredStyle: .alert)
                alert.addAction(acceptAction)
                self.present(alert, animated: true) {
                }
            }else{
             
                empleado.registros.append(tmp)
                
                let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                let alert = UIAlertController(title: "Aviso", message: "Usuario registrado exitosamente", preferredStyle: .alert)
                alert.addAction(acceptAction)
                self.present(alert, animated: true) {
                }
            }
        }
    }
    
    @objc func datePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        birthDayTextField.text = dateFormatter.string(from: sender.date)
        
    }
    
    func isValidEmail(str:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: str)
    }
    
    func usrExists(usr:String) -> Bool {
        
        for emp in empleado.registros{
            if emp.strUser == usr{
                return true
            }
        }
        return false
    }
    
    func emailExists(email:String) -> Bool {
        for emp in empleado.registros{
            if emp.strEmail == email{
                return true
            }
        }
        return false
    }
}
