//
//  ViewController.swift
//  Actividad1
//
//  Created by Macbook on 20/05/19.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let empleado : Empleado = Empleado.sharedInstance
    var showAlert : Bool = Bool()
    
    @IBOutlet weak var usrTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        var emp : Empleado = Empleado()
        emp.strNombre = "sergio"
        emp.strPwd = "123"
        emp.strEmail = "email@email.com"
        emp.strBirthday = "22-05-1993"
        emp.strEmpNumber = "64451"
        emp.strPhone = "5547835471"
        emp.strUser = "sergio"
        empleado.registros.append(emp)
        
        emp = Empleado()
        emp.strNombre = "sergio1"
        emp.strPwd = "123"
        emp.strEmail = "email@email.com"
        emp.strBirthday = "16-09-1993"
        emp.strEmpNumber = "64451"
        emp.strPhone = "5547835471"
        emp.strUser = "sergio1"
        empleado.registros.append(emp)
        
        emp = Empleado()
        emp.strNombre = "sergio0"
        emp.strPwd = "123"
        emp.strEmail = "email@email.com"
        emp.strBirthday = "16-09-1993"
        emp.strEmpNumber = "64451"
        emp.strPhone = "5547835471"
        emp.strUser = "sergio0"
        empleado.registros.append(emp)
        
        emp = Empleado()
        emp.strNombre = "sergio2"
        emp.strPwd = "123"
        emp.strEmail = "email@email.com"
        emp.strBirthday = "16-09-1993"
        emp.strEmpNumber = "64451"
        emp.strPhone = "5547835471"
        emp.strUser = "sergio2"
        empleado.registros.append(emp)
        
        emp = Empleado()
        emp.strNombre = "sergio3"
        emp.strPwd = "123"
        emp.strEmail = "email@email.com"
        emp.strBirthday = "16-09-1993"
        emp.strEmpNumber = "64451"
        emp.strPhone = "5547835471"
        emp.strUser = "sergio3"
        empleado.registros.append(emp)
        
        emp = Empleado()
        emp.strNombre = "sergio4"
        emp.strPwd = "123"
        emp.strEmail = "email@email.com"
        emp.strBirthday = "16-09-1993"
        emp.strEmpNumber = "64451"
        emp.strPhone = "5547835471"
        emp.strUser = "sergio4"
        empleado.registros.append(emp)
        
        emp = Empleado()
        emp.strNombre = "sergio5"
        emp.strPwd = "123"
        emp.strEmail = "email@email.com"
        emp.strBirthday = "16-09-1993"
        emp.strEmpNumber = "64451"
        emp.strPhone = "5547835471"
        emp.strUser = "sergio5"
        empleado.registros.append(emp)
    }
    
    @objc func dismissKeyboard(){
        
        view.endEditing(true)
    }
    
    @IBAction func btnLoginButtonTapped(_ sender: Any) {
        
        if empleado.registros.count != 0 {
            if usrTextField.text == ""{
                let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                }
                let alert = UIAlertController(title: "Aviso", message: "Introduce tu nombre de usuario", preferredStyle: .alert)
                alert.addAction(acceptAction)
                self.present(alert, animated: true) {
                }
            }else if pwdTextField.text == ""{
                let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                }
                let alert = UIAlertController(title: "Aviso", message: "Introduce tu contraseña", preferredStyle: .alert)
                alert.addAction(acceptAction)
                self.present(alert, animated: true) {
                }
            }else{
                showAlert = true
                for emp in empleado.registros{
                    if (emp.strUser == usrTextField.text || emp.strEmail == usrTextField.text) && emp.strPwd == pwdTextField.text{
                        showAlert = false
                        self.performSegue(withIdentifier: "dashboardSegue", sender: self)
                        break
                    }
                }
                
                if (showAlert){
                    
                    let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                    }
                    let alert = UIAlertController(title: "Aviso", message: "Usuario y/o password incorrectos", preferredStyle: .alert)
                    alert.addAction(acceptAction)
                    self.present(alert, animated: true) {
                    }
                }
            }
        }else{
            
            let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
            }
            let alert = UIAlertController(title: "Aviso", message: "No existe ningun usuario registrado, ve a la pantalla de registro", preferredStyle: .alert)
            alert.addAction(acceptAction)
            self.present(alert, animated: true) {
            }
        }
    }
}
