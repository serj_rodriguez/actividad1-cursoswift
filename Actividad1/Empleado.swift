//
//  Empleado.swift
//  Actividad1
//
//  Created by Macbook on 20/05/19.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class Empleado: NSObject {
    
    var strNombre : String! = ""
    var strPwd : String! = ""
    var strEmail : String! = ""
    var strBirthday : String! = ""
    var strEmpNumber : String! = ""
    var strPhone : String! = ""
    var strUser : String! = ""
    var registros : [Empleado] = [Empleado]()
    
    override init() {
    }
    
    func clean(){
        
        self.strNombre = ""
        self.strPwd = ""
        self.strEmail = ""
        self.strBirthday = ""
        self.strEmpNumber = ""
        self.strPhone  = ""
        self.strUser  = ""
    }
    
    static let sharedInstance = Empleado()
}
